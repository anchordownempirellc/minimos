#include <kernel/timer.h>
#include <kernel/common.h>
#include <kernel/pic.h>

#define PIT_CHANNEL0    0x40
#define PIT_CHANNEL1    0x41
#define PIT_CHANNEL2    0x42
#define PIT_COMMAND     0x43


uint32_t tick_count = 0;


__attribute__((interrupt))
void timer_interrupt(InterruptFrame *frame)
{
    tick_count++;
    PIC::send_eoi(static_cast<uint8_t>(Interrupt::timer));
}


uint32_t timer_tick_count()
{
    return tick_count;
}


void PIT::set_frequency(uint32_t frequency)
{
    uint32_t divisor;

    // Frequency limits: 18 Hz <= f <= 1193181 Hz
    if (frequency <= 18) {
        m_freq = 18;
        divisor = 0;    // Defaults to 18 Hz in PIT
    }
    else {
        if (frequency > 1193181) {
            m_freq = 1193181;
        }
        else {
            m_freq = frequency;
        }
        divisor = 1193181 / m_freq;
    }

    outb(PIT_COMMAND, 0x36);

    uint8_t lo = static_cast<uint8_t>(divisor & 0xFF);
    uint8_t hi = static_cast<uint8_t>((divisor >> 8) & 0xFF);

    outb(PIT_CHANNEL0, lo);
    outb(PIT_CHANNEL0, hi);
}
