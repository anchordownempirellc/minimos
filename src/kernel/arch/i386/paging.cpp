#include <kernel/paging.h>


unsigned int PageDirectory::get_entry_index(void *virt_addr) const
{
    return reinterpret_cast<unsigned int>(virt_addr) >> 22;
}


bool PageDirectory::is_pde_present(void *virt_addr) const
{
    unsigned pd_idx = get_entry_index(virt_addr);
    PageDirectory::Entry *pde = &m_entries[pd_idx];

    return pde->m_flags & 0x001;
}


bool PageDirectory::is_present(void *virt_addr) const
{
    if (!is_pde_present(virt_addr)) {
        return false;
    }

    unsigned pd_idx = get_entry_index(virt_addr);
    PageTable pt(pd_idx);
    return pt.is_pte_present(virt_addr);
}


void *PageDirectory::get_physical_addr(void *virt_addr) const
{
    if (!is_pde_present(virt_addr)) {
        return nullptr;
    }

    unsigned pd_idx = get_entry_index(virt_addr);
    PageTable pt(pd_idx);

    return pt.get_physical_addr(virt_addr);
}


void PageDirectory::map_addr(void *phys_addr, void *virt_addr, uint16_t flags)
{
    unsigned pd_idx = get_entry_index(virt_addr);
    PageDirectory::Entry *pde = &m_entries[pd_idx];

    if (!is_pde_present(virt_addr)) {
        pde->m_address = (reinterpret_cast<uint32_t>(&_page_table_physaddr) + 0x1000 * pd_idx) >> 12;
        pde->m_flags = 0x003;
    }

    PageTable pt(pd_idx);
    pt.map_addr(phys_addr, virt_addr, flags);

    // flush(); TODO: is this necessary?
}


void PageDirectory::unmap_addr(void *virt_addr)
{
    if (!is_pde_present(virt_addr)) {
        return;
    }

    unsigned pd_idx = get_entry_index(virt_addr);
    PageTable pt(pd_idx);
    pt.unmap_addr(virt_addr);

    // flush(); TODO: is this necessary?
}


void PageDirectory::flush() const
{
    // TODO: flush tlb rather than reloading cr3
    asm("movl %cr3, %ecx");
    asm("movl %ecx, %cr3");
}


PageTable::PageTable(unsigned int idx) :
    m_entries(reinterpret_cast<Entry *>(0xFFC00000 + 0x1000 * idx))
{

}


unsigned int PageTable::get_entry_index(void *virt_addr) const
{
    return reinterpret_cast<unsigned int>(virt_addr) >> 12 & 0x03FF;
}


bool PageTable::is_pte_present(void *virt_addr) const
{
    unsigned int pt_idx = get_entry_index(virt_addr);
    PageTable::Entry *pte = &m_entries[pt_idx];

    return pte->m_flags & 0x001;
}


void *PageTable::get_physical_addr(void *virt_addr) const
{
    if (!is_pte_present(virt_addr)) {
        return nullptr;
    }

    unsigned int pt_idx = get_entry_index(virt_addr);
    PageTable::Entry *pte = &m_entries[pt_idx];

    unsigned int page = (pte->m_address << 12) + (reinterpret_cast<unsigned int>(virt_addr) & 0xFFF);
    return reinterpret_cast<void *>(page);
}


void PageTable::map_addr(void *phys_addr, void *virt_addr, uint16_t flags)
{
    if (is_pte_present(virt_addr)) {
        return; // TODO: handle duplicate mapping
    }

    unsigned int pt_idx = get_entry_index(virt_addr);
    PageTable::Entry *pte = &m_entries[pt_idx];
    pte->m_address = reinterpret_cast<uint32_t>(phys_addr) >> 12;
    pte->m_flags = (flags & 0xFFF) | 0x001;
}


void PageTable::unmap_addr(void *virt_addr)
{
    if (!is_pte_present(virt_addr)) {
        return;
    }

    unsigned int pt_idx = get_entry_index(virt_addr);
    PageTable::Entry *pte = &m_entries[pt_idx];
    pte->m_flags &= ~0x001;
}
