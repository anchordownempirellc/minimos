#include <kernel/idt.h>


InterruptDescriptorTable::InterruptDescriptorTable()
{
    // Set IDT ptr for loading to CPU
    m_ptr.m_limit = IDT_SIZE * sizeof(InterruptDescriptorTable::Entry);
    m_ptr.m_base = m_entries;
}


InterruptDescriptorTable::Entry::Entry(uint32_t base, uint16_t selector, uint8_t type)
{
    m_base0_15 = base & 0xFFFF;
    m_base16_31 = (base >> 16) & 0xFFFF;
    m_selector = selector;
    m_type = type;
}


void InterruptDescriptorTable::set_entry(unsigned int index, InterruptDescriptorTable::Entry entry)
{
    m_entries[index] = entry;
}


// Loads the IDT ptr into the CPU and enables interrupts
void InterruptDescriptorTable::load()
{
    asm("lidtl (%%eax)" : : "a"(&m_ptr));
    asm("sti");
}

