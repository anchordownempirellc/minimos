#include <kernel/memory.h>
#include <stdio.h>

extern uint32_t _kernel_start;
extern uint32_t _kernel_end;
extern uint32_t _kernel_offset;


void Memory::init(multiboot_info_t *mbd)
{
    m_page_allocator.init(mbd);

    // Remove identity mapped kernel code
    uint32_t addr;
    for (uint32_t addr = 0x00100000; addr < 0x00200000; addr += 0x1000) {
        m_page_directory.unmap_addr(reinterpret_cast<void *>(addr));
    }

    // Remove unused mappings starting from end of address stack
    addr = reinterpret_cast<uint32_t>(&_kernel_end) + mbd->mem_lower / 4 + mbd->mem_upper / 4;
    addr = (addr + 0xFFF) & 0xFFFFF000; // round up to first unused page
    while (addr <= 0xFFFFF000 && m_page_directory.is_present(reinterpret_cast<void *>(addr))) {
        m_page_directory.unmap_addr(reinterpret_cast<void *>(addr));
        addr += 0x1000;
    }
}


void PageAllocator::init(multiboot_info_t *mbd)
{
    // Place our address stack immediately following the kernel
    m_address_stack.init(reinterpret_cast<void **>(&_kernel_end), mbd->mem_lower / 4,
                         reinterpret_cast<void **>(&_kernel_end) + mbd->mem_lower / 4, mbd->mem_upper / 4);

    // Push free physical pages onto the address stack
    uint32_t entry_addr = mbd->mmap_addr;
    while (entry_addr < mbd->mmap_addr + mbd->mmap_length) {
        multiboot_memory_map_t *entry = reinterpret_cast<multiboot_memory_map_t *>(entry_addr);

        if (entry->type == MULTIBOOT_MEMORY_AVAILABLE) {
            uint32_t addr = entry->addr_low;
            while (addr + 0x1000 <= entry->addr_low + entry->len_low) {
                // Only push pages outside the kernel
                if (addr < &_kernel_start - &_kernel_offset || addr > &_kernel_end - &_kernel_offset) {
                    m_address_stack.push_addr(reinterpret_cast<void *>(addr));
                }

                addr += 0x1000;
            }
        }

        entry_addr += entry->size + sizeof(entry->size);
    }
}


void *PageAllocator::alloc_page()
{
    void *page = m_address_stack.get_addr();
    m_address_stack.pop_addr();
    return page;
}


void PageAllocator::dealloc_page(void *page)
{
    m_address_stack.push_addr(page);
}


void AddressStack::init(void **lower_stack, unsigned int lower_capacity, void **upper_stack, unsigned int upper_capacity)
{
    m_lower_stack = lower_stack;
    m_lower_capacity = lower_capacity;
    m_upper_stack = upper_stack;
    m_upper_capacity = upper_capacity;
}


unsigned int AddressStack::lower_capacity() const
{
    return m_lower_capacity;
}


unsigned int AddressStack::capacity() const
{
    return m_lower_capacity + m_upper_capacity;
}


unsigned int AddressStack::lower_size() const
{
    return m_lower_size;
}


unsigned int AddressStack::size() const
{
    return m_lower_size + m_upper_size;
}


void *AddressStack::get_low_addr() const
{
    // TODO: assertion
    return m_lower_stack[m_lower_size - 1];
}


void *AddressStack::get_addr() const
{
    // TODO: assertion
    if (m_upper_size > 0) {
        return m_upper_stack[m_upper_size - 1];
    }
    return get_low_addr();
}


void AddressStack::pop_low_addr()
{
    // TODO: assertion
    m_lower_size--;
}


void AddressStack::pop_addr()
{
    // TODO: assertion
    if (m_upper_size > 0) {
        m_upper_size--;
    }
    else {
        pop_low_addr();
    }
}


bool AddressStack::push_addr(void *addr)
{
    // TODO: assertion
    if (addr < reinterpret_cast<void *>(0x100000)) {
        m_lower_stack[m_lower_size++] = addr;
    }
    else {
        m_upper_stack[m_upper_size++] = addr;
    }

    return true;
}
