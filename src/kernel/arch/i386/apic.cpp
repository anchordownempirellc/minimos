#include <kernel/pic.h>
#include <kernel/system.h>


APIC::APIC() :
    m_base(get_base())
{

}


uint32_t APIC::read_register(uint16_t reg)
{
    return m_base[reg];
}


void APIC::write_register(uint16_t reg, uint32_t val)
{
    m_base[reg] = val;
}


void APIC::set_base(uint32_t *ptr)
{
    cpu_set_msr(0x1B, (reinterpret_cast<uint32_t>(ptr) & 0xFFFFF000) | 0x800, 0);
}


uint32_t *APIC::get_base()
{
    uint32_t eax, edx;
    cpu_get_msr(0x1B, &eax, &edx);
    return reinterpret_cast<uint32_t *>(eax & 0xFFFFF000);
}


void APIC::enable()
{
    set_base(get_base());

    write_register(0xF0, read_register(0xF) | 0x100);
}

