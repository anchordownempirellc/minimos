#include <kernel/gdt.h>


GlobalDescriptorTable::GlobalDescriptorTable()
{
    // Set GDT ptr for loading to CPU
    m_ptr.m_limit = GDT_SIZE * sizeof(GlobalDescriptorTable::Entry);
    m_ptr.m_base = m_entries;
}


GlobalDescriptorTable::Entry::Entry(uint32_t base, uint32_t limit, uint8_t access, uint8_t flags)
{
    m_base0_15 = base & 0xFFFF;
    m_base16_23 = (base >> 16) & 0xFF;
    m_base24_31 = (base >> 24) & 0xFF;
    m_limit0_15 = limit & 0xFFFF;
    m_limit16_19 = (limit >> 16) & 0xF;
    m_access = access;
    m_flags = flags & 0xF;
}


void GlobalDescriptorTable::set_entry(unsigned int index, GlobalDescriptorTable::Entry entry)
{
    m_entries[index] = entry;
}


void GlobalDescriptorTable::load()
{
    gdt_flush(reinterpret_cast<uint32_t>(&m_ptr));
}

