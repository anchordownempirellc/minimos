#include <kernel/kernel.h>
#include <kernel/isr.h>
#include <stdio.h>
#include <kernel/timer.h>
#include <kernel/sleep.h>
#include <kernel/icxxabi.h>

#define VERSION_MAJOR   0
#define VERSION_MINOR   1
#define VERSION_PATCH   0

Kernel::Kernel()
{

}

Kernel::~Kernel()
{

}


void Kernel::init(multiboot_info_t *mbd, unsigned int magic)
{
    terminal_initialize();

    m_memory.init(mbd);
    printf("[  OK  ] Read memory map\n");
    init_gdt();
    printf("[  OK  ] Initialized GDT\n");
    init_interrupt_controller();
    init_idt();
    printf("[  OK  ] Initialized IDT\n");
    m_pit.set_frequency(1000);
    printf("[  OK  ] Set PIT to 1 kHz\n");
}


void Kernel::start()
{
    printf("Hello, world!\n");
    while (1) {
        sleep(1000);
    }
}


void Kernel::init_gdt()
{
    // Flat memory segmentation setup
    m_gdt.set_entry(0, GlobalDescriptorTable::Entry(0x0, 0x0, 0x0, 0x0));          // null descriptor
    m_gdt.set_entry(1, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0x9A, 0xC)); // code
    m_gdt.set_entry(2, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0x92, 0xC)); // data
    m_gdt.set_entry(3, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0xFA, 0xC)); // user code
    m_gdt.set_entry(4, GlobalDescriptorTable::Entry(0x0, 0xFFFFFFFF, 0xF2, 0xC)); // user data
    // TODO: Task state segment

    m_gdt.load();
}


void Kernel::init_idt()
{
    // 0x08 selector => Kernel code segment, GDT, ring 0
    // 0x8E type => present, 32-bit interrupt
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::divide_by_zero), InterruptDescriptorTable::Entry((uint32_t)divide_by_zero_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::debug), InterruptDescriptorTable::Entry((uint32_t)debug_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::nonmaskable_interrupt), InterruptDescriptorTable::Entry((uint32_t)nonmaskable_interrupt_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::breakpoint), InterruptDescriptorTable::Entry((uint32_t)breakpoint_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::overflow), InterruptDescriptorTable::Entry((uint32_t)overflow_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::bound_range_exceeded), InterruptDescriptorTable::Entry((uint32_t)bound_range_exceeded_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::invalid_opcode), InterruptDescriptorTable::Entry((uint32_t)invalid_opcode_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::device_not_available), InterruptDescriptorTable::Entry((uint32_t)device_not_available_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::double_fault), InterruptDescriptorTable::Entry((uint32_t)double_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::coprocessor_segment_overrun), InterruptDescriptorTable::Entry((uint32_t)coprocessor_segment_overrun_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::invalid_tss), InterruptDescriptorTable::Entry((uint32_t)invalid_tss_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::segment_not_present), InterruptDescriptorTable::Entry((uint32_t)segment_not_present_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::stack_segment_fault), InterruptDescriptorTable::Entry((uint32_t)stack_segment_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::general_protection_fault), InterruptDescriptorTable::Entry((uint32_t)general_protection_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::page_fault), InterruptDescriptorTable::Entry((uint32_t)page_fault_handler, 0x08, 0x8E));
    // Intel-reserved interrupt 15
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::math_fault), InterruptDescriptorTable::Entry((uint32_t)math_fault_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::alignment_check), InterruptDescriptorTable::Entry((uint32_t)alignment_check_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::machine_check), InterruptDescriptorTable::Entry((uint32_t)machine_check_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::simd_fpe), InterruptDescriptorTable::Entry((uint32_t)simd_fpe_handler, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::virtualization), InterruptDescriptorTable::Entry((uint32_t)virtualization_handler, 0x08, 0x8E));
    // Intel-reserved interrupts 21-31

    // Interrupt requests
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::timer), InterruptDescriptorTable::Entry((uint32_t)timer_interrupt, 0x08, 0x8E));
    m_idt.set_entry(static_cast<uint8_t>(Interrupt::keyboard), InterruptDescriptorTable::Entry((uint32_t)keyboard_interrupt, 0x08, 0x8E));

    m_idt.load();
}


void Kernel::init_interrupt_controller()
{
    m_pic.init();

    /*
    // TODO: Map this somewhere else
    m_page_directory.map_addr(
            reinterpret_cast<void *>(m_apic.get_base()),
            reinterpret_cast<void *>(m_apic.get_base()),
            0x003);
    m_apic.enable();
    
    m_apic.write_register(0x320, m_apic.read_register(0x320) | 0x01 & ~0x00010000);
    */
}
