.global _enable_apic
.type _enable_apic, @function
_enable_apic:
    mov $0x1B, %ecx
    rdmsr
    andl $0xFFFFF000, %eax
    orl $0x800, %eax
    wrmsr

