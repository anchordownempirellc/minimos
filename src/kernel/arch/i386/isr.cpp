#include <stdio.h>

#include <kernel/isr.h>
#include <kernel/common.h>
#include <kernel/pic.h>
#include <kernel/sleep.h>

__attribute__((interrupt))
void divide_by_zero_handler(InterruptFrame *frame)
{
    printf("Divide by zero error\n");
}

__attribute__((interrupt))
void debug_handler(InterruptFrame *frame)
{
    printf("Debug interrupt\n");
}

__attribute__((interrupt))
void nonmaskable_interrupt_handler(InterruptFrame *frame)
{
    printf("Non-maskable interrupt\n");
}

__attribute__((interrupt))
void breakpoint_handler(InterruptFrame *frame)
{
    printf("Breakpoint interrupt\n");
}

__attribute__((interrupt))
void overflow_handler(InterruptFrame *frame)
{
    printf("Overflow interrupt\n");
}

__attribute__((interrupt))
void bound_range_exceeded_handler(InterruptFrame *frame)
{
    printf("Bound range exceeded error\n");
}

__attribute__((interrupt))
void invalid_opcode_handler(InterruptFrame *frame)
{
    printf("Invalid opcode error\n");
}

__attribute__((interrupt))
void device_not_available_handler(InterruptFrame *frame)
{
    printf("Device not available error\n");
}

__attribute__((interrupt))
void double_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Double fault\n");
}

__attribute__((interrupt))
void coprocessor_segment_overrun_handler(InterruptFrame *frame)
{
    printf("Coprocessor segment overrun error\n");
}

__attribute__((interrupt))
void invalid_tss_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Invalid TSS exception\n");
}

__attribute__((interrupt))
void segment_not_present_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Segment not present exception\n");
}

__attribute__((interrupt))
void stack_segment_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Stack segment fault\n");
}

__attribute__((interrupt))
void general_protection_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("General protection fault\n");
}

__attribute__((interrupt))
void page_fault_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Page fault\n");
}

__attribute__((interrupt))
void math_fault_handler(InterruptFrame *frame)
{
    printf("Math fault\n");
}

__attribute__((interrupt))
void alignment_check_handler(InterruptFrame *frame, uint32_t err_code)
{
    printf("Alignment check exception\n");
}

__attribute__((interrupt))
void machine_check_handler(InterruptFrame *frame)
{
    printf("Machine check interrupt\n");
}

__attribute__((interrupt))
void simd_fpe_handler(InterruptFrame *frame)
{
    printf("SIMD floating-point error\n");
}

__attribute__((interrupt))
void virtualization_handler(InterruptFrame *frame)
{
    printf("Virtualization error\n");
}


__attribute__((interrupt))
void keyboard_interrupt(InterruptFrame *frame)
{
    printf("Keyboard interrupt\n");
    int a = inb(0x60);
    PIC::send_eoi(static_cast<uint8_t>(Interrupt::keyboard));
}
