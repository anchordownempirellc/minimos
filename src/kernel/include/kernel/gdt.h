#ifndef _GDT_H
#define _GDT_H

#include <stdint.h>

#define GDT_SIZE 5


class GlobalDescriptorTable
{
public:
    struct Entry {
        uint16_t m_limit0_15;
        uint16_t m_base0_15;
        uint8_t m_base16_23;
        uint8_t m_access;
        uint8_t m_limit16_19:4;
        uint8_t m_flags:4;
        uint8_t m_base24_31;

        Entry() = default;
        Entry(uint32_t base, uint32_t limit, uint8_t access, uint8_t flags);
    }__attribute__((packed));
private:
    struct GDTPtr {
        uint16_t m_limit;
        Entry *m_base;
    }__attribute__((packed));

    GDTPtr m_ptr;
    Entry m_entries[GDT_SIZE];
public:
    GlobalDescriptorTable();

    void set_entry(unsigned int index, Entry entry);
    void load();
};

extern void gdt_flush(uint32_t);

#endif
