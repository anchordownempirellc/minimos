#ifndef _ISR_H
#define _ISR_H

#include <stdint.h>


struct InterruptFrame
{
    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    // TODO: Add fields for crossing from ring 3 to ring 0
};

enum class Interrupt: uint8_t
{
    divide_by_zero = 0,
    debug = 1,
    nonmaskable_interrupt = 2,
    breakpoint = 3,
    overflow = 4,
    bound_range_exceeded = 5,
    invalid_opcode  = 6,
    device_not_available = 7,
    double_fault = 8,
    coprocessor_segment_overrun = 9,
    invalid_tss = 10,
    segment_not_present = 11,
    stack_segment_fault = 12,
    general_protection_fault = 13,
    page_fault = 14,
    math_fault = 16,
    alignment_check = 17,
    machine_check = 18,
    simd_fpe = 19,
    virtualization = 20,

    timer = 32,
    keyboard = 33
};

void divide_by_zero_handler(InterruptFrame *frame);
void debug_handler(InterruptFrame *frame);
void nonmaskable_interrupt_handler(InterruptFrame *frame);
void breakpoint_handler(InterruptFrame *frame);
void overflow_handler(InterruptFrame *frame);
void bound_range_exceeded_handler(InterruptFrame *frame);
void invalid_opcode_handler(InterruptFrame *frame);
void device_not_available_handler(InterruptFrame *frame);
void double_fault_handler(InterruptFrame *frame, uint32_t err_code);
void coprocessor_segment_overrun_handler(InterruptFrame *frame);
void invalid_tss_handler(InterruptFrame *frame, uint32_t err_code);
void segment_not_present_handler(InterruptFrame *frame, uint32_t err_code);
void stack_segment_fault_handler(InterruptFrame *frame, uint32_t err_code);
void general_protection_handler(InterruptFrame *frame, uint32_t err_code);
void page_fault_handler(InterruptFrame *frame, uint32_t err_code);
void math_fault_handler(InterruptFrame *frame);
void alignment_check_handler(InterruptFrame *frame, uint32_t err_code);
void machine_check_handler(InterruptFrame *frame);
void simd_fpe_handler(InterruptFrame *frame);
void virtualization_handler(InterruptFrame *frame);

void keyboard_interrupt(InterruptFrame *frame);

#endif
