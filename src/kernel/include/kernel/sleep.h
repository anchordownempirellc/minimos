#ifndef _SLEEP_H
#define _SLEEP_H

#include <stdint.h>

void sleep(uint32_t ticks);

#endif
