#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stddef.h>

/* Initialize the terminal by setting static variables and clearing the screen */
void terminal_initialize(void);
/* Place the character c at the cursor */
void terminal_putchar(char c);
/* Write the given data to the terminal, starting at the cursor */
void terminal_write(const char *data, size_t size);
/* Write the string to the terminal, starting at the cursor */
void terminal_writestring(const char *data);

#endif
