#ifndef _PIC_H
#define _PIC_H

#include <stdint.h>

#define PIC1            0x20        // IO base addr for master PIC.
#define PIC2            0xA0        // IO base addr for slave PIC
#define PIC1_COMMAND    PIC1
#define PIC2_COMMAND    PIC2
#define PIC1_DATA       (PIC1+1)
#define PIC2_DATA       (PIC2+1)

#define PIC_EOI         0x20

#define IA32_APIC_BASE_MSR          0x1B
#define IA32_APIC_BASE_MSR_BSP      0x100
#define IA32_APIC_BASE_MSR_ENABLE   0x800

class PIC
{
private:
    static uint8_t m_offset1;
    static uint8_t m_offset2;

    void remap(uint32_t offset1, uint32_t offset2);
public:
    PIC() = default;

    void init();
    void enable();
    void disable();

    void set_mask(uint8_t irq);
    void clear_mask(uint8_t irq);
    static void send_eoi(uint8_t irq);
};

class APIC
{
private:
    uint32_t *m_base;
public:
    APIC();

    uint32_t read_register(uint16_t reg);
    void write_register(uint16_t reg, uint32_t val);

    void set_base(uint32_t *ptr);
    uint32_t *get_base();
    void enable();
};

#endif
