#ifndef _KERNEL_H
#define _KERNEL_H

#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/memory.h>
#include <kernel/multiboot.h>
#include <kernel/pic.h>
#include <kernel/timer.h>
#include <kernel/tty.h>


class Kernel
{
private:
    Memory m_memory;
    GlobalDescriptorTable m_gdt;
    InterruptDescriptorTable m_idt;
    PIC m_pic;
    APIC m_apic;
    PIT m_pit;

    void init_gdt();
    void init_idt();
    void init_interrupt_controller();
public:
    Kernel();
    ~Kernel();

    void init(multiboot_info_t *mbd, unsigned int magic);
    void start();
};

#endif

