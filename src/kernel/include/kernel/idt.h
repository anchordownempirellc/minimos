#ifndef _IDT_H
#define _IDT_H

#include <stdint.h>

#define IDT_SIZE 256


class InterruptDescriptorTable
{
public:
    struct Entry {
        uint16_t m_base0_15;
        uint16_t m_selector;
        uint8_t m_zero = 0;
        uint8_t m_type;
        uint16_t m_base16_31;

        Entry() = default;
        Entry(uint32_t base, uint16_t selector, uint8_t type);
    }__attribute__((packed));
private:
    struct IDTPtr {
        uint16_t m_limit;
        Entry *m_base;
    }__attribute__((packed));

    IDTPtr m_ptr;
    Entry m_entries[IDT_SIZE];
public:
    InterruptDescriptorTable();

    void set_entry(unsigned int index, Entry entry);
    void load();
};

#endif

