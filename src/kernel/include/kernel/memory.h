#ifndef _MEMORY_H
#define _MEMORY_H

#include <kernel/multiboot.h>
#include <kernel/paging.h>

#define KERNEL_HEAP_LOCATION    0xD0000000


// TODO: Protect with mutex once multithreaded
class AddressStack
{
private:
    void **m_lower_stack;   // stack for addresses below 1 MiB
    void **m_upper_stack;   // stack for address above 1 MiB

    unsigned int m_lower_capacity;
    unsigned int m_upper_capacity;
    unsigned int m_lower_size;
    unsigned int m_upper_size;
public:
    AddressStack() = default;

    void init(void **lower_stack, unsigned int lower_capacity, void **upper_stack, unsigned int upper_capacity);

    unsigned int lower_capacity() const;
    unsigned int capacity() const;
    unsigned int lower_size() const;
    unsigned int size() const;
    void *get_low_addr() const;
    void *get_addr() const;
    void pop_low_addr();
    void pop_addr();
    bool push_addr(void *addr);
};


class PageAllocator
{
private:
    AddressStack m_address_stack;
public:
    PageAllocator() = default;

    void init(multiboot_info_t *mbd);
    void *alloc_page();
    void dealloc_page(void *page);
};


class Memory
{
private:
    PageAllocator m_page_allocator;
    PageDirectory m_page_directory;
public:
    Memory() = default;

    void init(multiboot_info_t *mbd);
};

#endif
