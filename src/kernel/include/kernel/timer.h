#ifndef _TIMER_H
#define _TIMER_H

#include <kernel/isr.h>
#include <stdint.h>


void timer_interrupt(InterruptFrame *frame);

uint32_t timer_tick_count();


class PIT
{
private:
    uint32_t m_freq;
public:
    PIT() = default;
    ~PIT() = default;

    void set_frequency(uint32_t frequency);

    inline uint32_t frequency() const { return m_freq; }
};

#endif
