#ifndef _PAGING_H
#define _PAGING_H

#include <stdint.h>

extern uint32_t _page_directory_physaddr;
extern uint32_t _page_table_physaddr;
#define PAGE_TABLE_SIZE     1024
#define PAGE_DIRECTORY_SIZE 1024


class PageDirectory
{
public:
    struct Entry {
        uint32_t m_flags:12;
        uint32_t m_address:20;
    }__attribute__((packed));
private:
    Entry *m_entries = reinterpret_cast<Entry *>(0xFFFFF000);

    unsigned int get_entry_index(void *virt_addr) const;
public:
    PageDirectory() = default;

    bool is_present(void *virt_addr) const;
    bool is_pde_present(void *virt_addr) const;
    void *get_physical_addr(void *virt_addr) const;
    void map_addr(void *phys_addr, void *virt_addr, uint16_t flags);
    void unmap_addr(void *virt_addr);
    void flush() const;
};


class PageTable
{
public:
    struct Entry {
        uint32_t m_flags:12;
        uint32_t m_address:20;
    }__attribute__((packed));
private:
    Entry *m_entries;

    unsigned int get_entry_index(void *virt_addr) const;
public:
    PageTable(unsigned int idx);

    bool is_pte_present(void *virt_addr) const;
    void *get_physical_addr(void *virt_addr) const;
    void map_addr(void *phys_addr, void *virt_addr, uint16_t flags);
    void unmap_addr(void *virt_addr);
};

#endif
