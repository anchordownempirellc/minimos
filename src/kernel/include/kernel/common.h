#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>


static inline void outb(uint16_t port, uint8_t data)
{
    asm volatile ("outb %1, %0" : : "dN" (port), "a" (data));
}


static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ("inb %1, %0" : "=a" (ret) : "dN" (port));
    return ret;
}

#endif
