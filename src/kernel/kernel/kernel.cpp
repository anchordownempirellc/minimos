#include <kernel/kernel.h>
#include <kernel/multiboot.h>


Kernel kernel;

/* Kernel entry point */
extern "C"
void kernel_main(multiboot_info_t* mbd, unsigned int magic)
{
    kernel.init(mbd, magic);
    kernel.start();
}

