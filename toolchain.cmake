set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR i386)

set(SYSROOT ${CMAKE_CURRENT_LIST_DIR}/sysroot)
file(MAKE_DIRECTORY ${SYSROOT})
set(ARCH_DIR ${CMAKE_CURRENT_LIST_DIR}/src/kernel/arch/${CMAKE_SYSTEM_PROCESSOR})

set(crti ${ARCH_DIR}/crti.o)
set(crtn ${ARCH_DIR}/crtn.o)

set(tools $ENV{CROSS_COMPILER})
set(tool_target $ENV{TARGET})
set(CMAKE_FIND_ROOT_PATH ${tools})
set(CMAKE_C_COMPILER ${tools}/bin/${tool_target}-gcc)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED True)
set(CMAKE_CXX_COMPILER ${tools}/bin/${tool_target}-g++)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_ASM_COMPILER ${tools}/bin/${tool_target}-gcc)
set(CMAKE_AR ${tools}/bin/${tool_target}-ar)
set(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> rcs <TARGET> <LINK_FLAGS> <OBJECTS>")
set(CMAKE_LD ${tools}/bin/${tool_target}-ld)

execute_process(
    COMMAND
        bash "-c" "${CMAKE_ASM_COMPILER} -c ${ARCH_DIR}/crti.S -o ${crti}"
)
execute_process(
    COMMAND
        bash "-c" "${CMAKE_ASM_COMPILER} -c ${ARCH_DIR}/crtn.S -o ${crtn}"
)
execute_process(
    COMMAND
        bash "-c" "${CMAKE_C_COMPILER} -print-file-name=crtbegin.o | tr -d '\n'"
    OUTPUT_VARIABLE
        crtbegin
)
execute_process(
    COMMAND
        bash "-c" "${CMAKE_C_COMPILER} -print-file-name=crtend.o | tr -d '\n'"
    OUTPUT_VARIABLE
        crtend
)

set(CMAKE_FIND_ROOT_PATH ${SYSROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(C_FLAGS --sysroot=${SYSROOT} -isystem=/usr/include -g -ffreestanding -Wall -Wextra -D__is_libc -mno-80387 -mgeneral-regs-only)
set(CXX_FLAGS ${C_FLAGS} -std=c++14 -fno-exceptions -fno-rtti)
set(ASM_FLAGS ${C_FLAGS})
add_compile_options(
    "$<$<COMPILE_LANGUAGE:C>:${C_FLAGS}>"
    "$<$<COMPILE_LANGUAGE:CXX>:${CXX_FLAGS}>"
    "$<$<COMPILE_LANGUAGE:ASM>:${ASM_FLAGS}>"
)

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> -nostdlib <LINK_FLAGS> ${crti} ${crtbegin} <OBJECTS> -o <TARGET> <LINK_LIBRARIES> ${crtend} ${crtn}")
set(CMAKE_CXX_LINK_EXECUTABLE ${CMAKE_C_LINK_EXECUTABLE})

