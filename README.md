# MinimOS
## Description
A small kernel written in C/C++ for fun/learning.

Some code borrowed from [osdev.org](www.osdev.org), as new features are tested/implemented for the first time.

## Instructions
1. Clone the repository and cd into it

   `git clone https://gitlab.com/jonathan.davis/minimos.git && cd minimos`
2. Configure `./compile.sh` to contain the path to your cross-compiler
3. Run `./compile.sh`
   This will generate build/ and sysroot/ and compile the kernel
4. Run `./iso.sh`
   This will generate an .iso file
5. Start the kernel with `./qemu.sh`

After starting the kernel, you should be presented with a screen saying "Hello, world!"

## File Structure
* __[Root Files](#root-files)__
  Main scripts
* __[isodir/](#isodir)__
  Contains the GRUB config and the kernel file
  * __[boot/](#isodirboot)__
    Boot files
    * __[grub/](#isodirbootgrub)__
      GRUB config
* __[src/](#src)__
  Source code for the entire operating system
  * __[kernel/](#srckernel)__
    Source code for the kernel
    * __[arch/](#srckernelarch)__
      Architecture specific source
      * __[i386/](#srckernelarchi386)__
        i386 architecture source
    * __[include/](#srckernelinclude)__
      * __[kernel/](#srckernelincludekernel)__
        Kernel level includes
    * __[kernel/](#srckernelkernel)__
      Common source
  * __[libc/](#srclibc)__
    Source code for the C standard library
* __[sysroot](#sysroot)__
  File system root for the kernel
  * __[boot/](#sysrootboot)__
  Boot directory for the kernel
  * __[usr/](#sysrootusr)__
  usr directory for system libraries and headers

## File Descriptions
### Root Files
* __CMakeLists.txt__
  Primary CMakeLists for the system
* __compile.sh__
  Compiles the kernel
* __iso.sh__
  Generates the .iso file
* __qemu.sh__
  Runs the .iso file in QEMU
* __[isodir](#isodir)__
* __[src](#src)__
* __[sysroot](#sysroot)__

### isodir/
* __[boot/](#isodirboot)__

### isodir/boot
* __minimos.kernel__
  Compiled kernel file
* __[grub/](#isodirbootgrub)__

### isodir/boot/grub
* __grub.cfg__
  GRUB config file

### src/
* __[kernel/](#srckernel)__
* __[libc/](#srclibc)__

### src/kernel
* __minimos.kernel__
  Compiled kernel file
* __[arch/](#srckernelarch)__
* __[include/](#srckernelinclude)__
* __[kernel/](#srckernelkernel)__

### src/kernel/arch
* __[i386/](#srckernelarchi386)__

### src/kernel/arch/i386
* __boot.S__
  Assembly for booting MinimOS
* __common.c__
* __common.h__
* __crti.S__
  C runtime prolog
* __crtn.S__
  C runtime epilog
* __desc_tab.S__
  Description tables (GDT, IDT)
* __gdt.c__
  Global descriptor table implementation
* __gdt.h__
  Global descriptor table header
* __idt.c__
  Interrupt descriptor table implementation
* __idt.h__
  Interrupt descriptor table header
* __isr.c__
  Interrupt service routine implementation
* __isr.h__
  Interrupt service routine header
* __linker.ld__
  Linker configuration for kernel sections
* __make.config__
* __pic.c__
  Programmable interrupt controller implementation
* __pic.h__
  Programmable interrupt controller header
* __system.c__
  System specific functions
* __timer.c__
  Timer implementation
* __timer.h__
  Timer header
* __tty.c__
  Terminal implementation
* __vga.h__
  VGA definitions

### src/kernel/include/
* __[kernel/](#srckernelincludekernel)__

### src/kernel/include/kernel
* __system.h__
  System headers
* __tty.h__
  Terminal headers

### src/kernel/kernel
* __kernel.c__
  Kernel implementation

### src/libc
* __[arch/](#srclibcarch)__
* __[include/](#srclibcinclude)__
* __[stdio/](#srclibcstdio)__
* __[stdlib/](#srclibcstdlib)__
* __[string/](#srclibcstring)__

### sysroot/
* __[boot/](#sysrootboot)__
* __[usr/](#sysrootusr)__

### sysroot/boot
* __minimos.kernel__
  Compiled kernel

### sysroot/usr
* __[include/](#sysrootusrinclude)__
* __[lib/](#sysrootusrlib)__

### sysroot/usr/include
System headers

### sysroot/usr/lib
System libraries
