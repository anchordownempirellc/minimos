cmake_minimum_required(VERSION 3.10)

set(CMAKE_OSX_SYSROOT "")

project(MinimOS VERSION 0.1.0 LANGUAGES C CXX ASM)

set(SYS_INCLUDES ${SYSROOT}/usr/include)
file(MAKE_DIRECTORY ${SYS_INCLUDES})
set(SYS_BOOT ${SYSROOT}/boot)
file(MAKE_DIRECTORY ${SYS_BOOT})

add_subdirectory(src/kernel)
add_subdirectory(src/libc)

